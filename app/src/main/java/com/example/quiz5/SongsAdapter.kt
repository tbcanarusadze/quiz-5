package com.example.quiz5

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.band_songs_list_layout.view.*

class SongsAdapter(private val items: MutableList<Bands.Songs>, private val itemOnClick: ItemOnClick) :
    RecyclerView.Adapter<SongsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.band_songs_list_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.onBind()
    }

    override fun getItemCount() = items.size

    private lateinit var model: Bands.Songs

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        fun onBind() {
            model = items[adapterPosition]
            itemView.songTextView.text = model.title
            itemView.setOnClickListener(this)

        }

        override fun onClick(v: View?) {
            itemOnClick.onItemClick(adapterPosition)
        }
    }
}
