package com.example.quiz5

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.bands_recyclerview_layout.view.*

class BandsAdapter(private val items: List<Bands>, private val itemOnClick: ItemOnClick) :
    RecyclerView.Adapter<BandsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.bands_recyclerview_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.onBind()
    }

    override fun getItemCount() = items.size

    private lateinit var model: Bands

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        fun onBind() {
            model = items[adapterPosition]
            itemView.bandNameTextView.text = model.name
            itemView.setOnClickListener(this)

            if(model.imageUrl!!.isEmpty()){
                itemView.imageView.setImageResource(R.mipmap.ic_launcher)
            }else{
                Glide.with(itemView.context).load(model.imageUrl).into(itemView.imageView)
            }
        }

        override fun onClick(v: View?) {
            itemOnClick.onItemClick(adapterPosition)
        }
    }

}

