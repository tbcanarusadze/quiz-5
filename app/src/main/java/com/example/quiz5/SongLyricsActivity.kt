package com.example.quiz5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_song_lyrics.*

class SongLyricsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_song_lyrics)
        init()
    }

    private fun init() {
        val artist = intent.extras!!.getString("band name")
        val song = intent.extras!!.getString("song title")
        d("artist", artist!!)
        d("title", song!!)
        create(artist, song)
    }


        private fun create(artist: String, song:String) {
            HttpRequest.getLyricsRequest(artist, song, object : CustomCallback {
                //
                override fun onFailure(response: String) {
                    Toast.makeText(
                        applicationContext,
                        "Error while loading page",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

                //
                override fun onResponse(response: String) {
//                val json = JSONObject(response)
//                if(json.has("lyrics"))
//                    lyricsTextView.text = json.getString("lyrics")
                    val lyrics = Gson().fromJson(response, Lyrics::class.java)
                    lyricsTextView.text = lyrics.lyrics


                }
            })
        }
    }
