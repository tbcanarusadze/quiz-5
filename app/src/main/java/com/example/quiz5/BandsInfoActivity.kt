package com.example.quiz5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_bands_info.*
import kotlinx.android.synthetic.main.activity_main.recyclerView
import kotlinx.android.synthetic.main.band_songs_list_layout.*
import org.json.JSONObject

class BandsInfoActivity : AppCompatActivity() {
    private lateinit var adapter: SongsAdapter
    private var listOfSongs = mutableListOf<Bands.Songs>()
    private var bands = mutableListOf<Bands>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bands_info)
        init()
        request(HttpRequest.INFO)
    }

    private fun init() {
        adapter = SongsAdapter(listOfSongs, object : ItemOnClick {
            override fun onItemClick(position: Int) {
                val intent = Intent(this@BandsInfoActivity, SongLyricsActivity::class.java)
//                intent.putExtra("bandDetails", listOfSongs[position])
                intent.putExtra("band name", Bands().name!![position])
                intent.putExtra("song title", songTextView.text.toString())
                startActivity(intent)
            }
        })
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        val band = intent.extras!!.getParcelable<Bands>("bandDetails")
        infoTextView.text = band!!.info
        bandTiTleTextView.text = band.name
    }

    private fun request(path: String) {
        HttpRequest.getRequest(path, object : CustomCallback {
            override fun onFailure(response: String) {
                Toast.makeText(applicationContext, "Error while loading page", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(response: String) {
                parseJson(response)
                adapter.notifyDataSetChanged()

            }
        })
    }


    private fun parseJson(response: String) {

        val json = JSONObject(response)
        val data = json.getJSONArray("data")
        for (i in 0 until data.length()) {
            val mObject = data.getJSONObject(i)
            val songsJSONArray = mObject.getJSONArray("songs")
            for (item in 0 until songsJSONArray.length()) {
                val songs = Bands.Songs()
                val oneSong = songsJSONArray[item] as JSONObject
                songs.title = oneSong.getString("title")
                listOfSongs.add(songs)
            }
//            bands.add(listOfSongs)
//            d("tag" ,"$bands")
//        val response = Gson().fromJson(response, BandData::class.java)
//        d("giorgigiorgi", response.data[0].songs[0].title)
//
//        bands.forEach(){
//            for(i in 0 until response.data.size){
//                if (it.name == response.data[i].band){
//                    it.songs.clear()
//                    it.songs.addAll(response.data[i].songs)
//                }
//            }
//        }
//        adapter.notifyDataSetChanged()





        }
    }}





