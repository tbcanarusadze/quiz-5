package com.example.quiz5

import android.os.Parcel
import android.os.Parcelable

class Bands(
    var name: String? = "",
    var imageUrl: String? = "",
    var info: String? = "",
    var genre: String? = "",
    var songs: MutableList<Songs> = mutableListOf<Songs>()

) : Parcelable {
    class Songs {
        var title: String = ""

    }

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(imageUrl)
        parcel.writeString(info)
        parcel.writeString(genre)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Bands> {
        override fun createFromParcel(parcel: Parcel): Bands {
            return Bands(parcel)
        }

        override fun newArray(size: Int): Array<Bands?> {
            return arrayOfNulls(size)
        }
    }


}

class BandData {

    var data = mutableListOf<Data>()

    class Data {
        var band = ""
        var songs = mutableListOf<Bands.Songs>()
    }
}

class Lyrics {
    var lyrics = ""
}










