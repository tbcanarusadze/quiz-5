package com.example.quiz5

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path


object HttpRequest {

    const val BANDS = "5ec3ab0f300000850039c29e"
    const val INFO = "5ec3ca1c300000e5b039c407"
    private const val bandsBaseUrl= "http://www.mocky.io/v2/"
    private const val lyricsBaseUrl = "https://api.lyrics.ovh/v1/"

    private var retrofitForBands = Retrofit.Builder()
        .baseUrl(bandsBaseUrl)
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    private var serviceForBands = retrofitForBands.create(ApiService::class.java)

    private var retrofitForLyrics = Retrofit.Builder()
        .baseUrl(lyricsBaseUrl)
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    private var serviceForLyrics = retrofitForLyrics.create(ApiService::class.java)

    interface ApiService {
        @GET("{path}")
        fun getRequest(@Path("path") competitions: String): Call<String>

        @GET("{path}/{child}")
        fun getSecondRequest(
            @Path("path") path: String, @Path("child") child: String
        ): Call<String>
    }

    fun getRequest(path: String, callback: CustomCallback){
        val call = serviceForBands.getRequest(path)
        call.enqueue(onCallback(callback))
    }

    fun getLyricsRequest(artist: String, song: String, callback: CustomCallback) {
        val call = serviceForLyrics.getSecondRequest(artist, song)
        call.enqueue(onCallback(callback))
    }


    private fun onCallback(callback: CustomCallback) = object : Callback<String>{
        override fun onFailure(call: Call<String>, t: Throwable) {
            callback.onFailure(t.message.toString())
        }

        override fun onResponse(call: Call<String>, response: Response<String>) {
            callback.onResponse(response.body().toString())

        }

    }
}