package com.example.quiz5

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.quiz5.HttpRequest.BANDS
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {
    private lateinit var adapter: BandsAdapter
    private var listOfBands = ArrayList<Bands>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        request(BANDS)
    }

    private fun init() {

        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = true
            refresh()

            Handler().postDelayed({
                swipeRefreshLayout.isRefreshing = false
                request(BANDS)

            }, 2000)
        }

    }

    private fun refresh() {
        adapter.notifyDataSetChanged()
        loadingTextView.visibility = View.VISIBLE
    }

    private fun request(path: String) {
        HttpRequest.getRequest(path, object : CustomCallback {
            override fun onFailure(response: String) {
                Toast.makeText(applicationContext, "Error while loading page", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(response: String) {
                parseJson(response)
                adapter.notifyDataSetChanged()
                loadingTextView.visibility = View.GONE

            }
        })
    }


    private fun parseJson(response: String) {
        val json = JSONArray(response)
        for (item in 0 until json.length()){
            val data =json[item] as JSONObject
            val bands = Bands()
            bands.name = data.getString("name")
            bands.imageUrl = data.getString("img_url")
            bands.info = data.getString("info")
            listOfBands.add(bands)
        }
        adapter = BandsAdapter(listOfBands, object : ItemOnClick {

            override fun onItemClick(position: Int) {
                adapter.notifyDataSetChanged()
                val intent = Intent(this@MainActivity, BandsInfoActivity::class.java)
                intent.putExtra("bandDetails", listOfBands[position])
                startActivity(intent)


            }
        })
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        loadingTextView.visibility = View.VISIBLE


    }
}




